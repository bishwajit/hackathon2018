package com.bishwajitdey.deiteo.executor;

import java.io.Serializable;
import java.util.List;

public class SensitivityPredictionConfig implements Serializable {
    private String inputDatabase = "sensitivo";
    private String inputTableName;
    private String tokenModel = "en-token.bin";
    private double probabilityThreshold = 0.5;
    private List<String> models;

    public SensitivityPredictionConfig() {
    }

    public String getInputTableName() {
        return inputTableName;
    }

    public String getTokenModel() {
        return tokenModel;
    }

    public double getProbabilityThreshold() {
        return probabilityThreshold;
    }

    public List<String> getModels() {
        return models;
    }

    public String getInputDatabase() {
        return inputDatabase;
    }
}
