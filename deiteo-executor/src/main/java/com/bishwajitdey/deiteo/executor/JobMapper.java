package com.bishwajitdey.deiteo.executor;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class JobMapper implements ResultSetMapper<Job> {

    @Override
    public Job map(int i, ResultSet r, StatementContext statementContext)
            throws SQLException {
        return new Job(r.getInt("id"), JobType.valueOf(r.getString("job_name")),
                r.getString("status"), r.getString("configuration"));
    }
}
