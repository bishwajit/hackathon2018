package com.bishwajitdey.deiteo.executor;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.databind.ObjectMapper;

import opennlp.tools.util.Span;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.FilterFunction;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.skife.jdbi.v2.DBI;
import org.skife.jdbi.v2.Handle;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ExecutorApp {
    private static final JsonFactory FACTORY = new JsonFactory();
    private static final ObjectMapper MAPPER = new ObjectMapper(FACTORY);
    private static final String DB_NAME = "sensitivo";
    private static final String TABLE_NAME = "job";

    public static void main(String[] args) {
        final DBI dbi = new DBI("jdbc:mysql://10.11.13.106:3306/" + DB_NAME, "root", "zaloni.1234");
        final Handle handle = dbi.open();

        final ScheduledExecutorService executorService = Executors.newScheduledThreadPool(1);
        final SparkSession spark = SparkSession
                .builder()
                .appName("Sensitivo Data Discovery")
                //.config("spark.sql.warehouse.dir", "")
                .enableHiveSupport()
                //.master("local[2]")
                .getOrCreate();

        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                executorService.shutdown();
                spark.stop();
                handle.close();
            }
        });

        executorService.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                System.out.println("Searching for new analytics job...");
                try {
                    Job job = getNextJobConfig(handle);
                    if (job == null) {
                        return;
                    }

                    JobType jobType = job.getJobType();
                    if (jobType == null) {
                        return;
                    }

                    System.out.println("Executing job " + job.toString());
                    switch (jobType) {
                        case SENSITIVITY_PREDICTION:
                            runSensitivityPredictionJob(spark, job, handle);
                            break;
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }, 0, 10, TimeUnit.SECONDS);
    }

    private static Job getNextJobConfig(Handle handle) {
        Job job = handle.createQuery("select * from " + TABLE_NAME + " where status = :status order by submit_time asc limit 1")
                .bind("status", "PENDING")
                .map(new JobMapper())
                .first();
        return job;
    }

    private static void setJobStatus(Handle handle, int id, String status) {
        handle.createStatement("update " + TABLE_NAME + " set status=:status, end_time=now() where id = :id")
                .bind("id", id)
                .bind("status", status).execute();
    }

    private static void runSensitivityPredictionJob(final SparkSession spark, Job job, Handle handle)
            throws Exception {
        try {
            setJobStatus(handle, job.getId(), "STARTED");
            final SensitivityPredictionConfig config = MAPPER.readValue(
                    job.getConfiguration(),
                    SensitivityPredictionConfig.class);
            final List<String> modelNames = config.getModels();

            Dataset<Row> data = spark.sql("SELECT * FROM " + config.getInputDatabase()
                    + "." + config.getInputTableName());
            final String[] fieldNames = data.schema().fieldNames();
            JavaRDD<Row> attributeValue = data.javaRDD().flatMap(new FlatMapFunction<Row, Row>() {
                @Override
                public Iterator<Row> call(Row row) throws Exception {
                    List<Row> attributeValues = new ArrayList<>();
                    for (String fieldName : fieldNames) {
                        if (!row.isNullAt(row.fieldIndex(fieldName))) {
                            attributeValues.add(RowFactory.create(config.getInputDatabase(),
                                    config.getInputTableName(), fieldName,
                                    row.get(row.fieldIndex(fieldName))));
                        }
                    }
                    return attributeValues.iterator();
                }
            });

            JavaRDD<Row> modelProbability = attributeValue.mapPartitions(
                    new FlatMapFunction<Iterator<Row>, Row>() {
                        @Override
                        public Iterator<Row> call(Iterator<Row> rowsPerPartition) throws Exception {
                            List<Model> models = new ArrayList<>(modelNames.size());
                            for (String modelName : modelNames) {
                                models.add(Model.createModel(modelName, modelName, config.getTokenModel()));
                            }

                            List<Row> rows = new LinkedList<>();
                            while (rowsPerPartition.hasNext()) {
                                Row row = rowsPerPartition.next();
                                for (Model model : models) {
                                    final String tokens[] = model.getTokenizer().tokenize(
                                            row.get(3).toString());
                                    final Span nameSpans[] = model.getNameFinder().find(tokens);
                                    final double[] spanProbs = model.getNameFinder().probs(nameSpans);
                                    for (int i = 0; i < nameSpans.length; i++) {
                                        rows.add(RowFactory.create(config.getInputDatabase(),
                                                config.getInputTableName(), row.get(2), model.getName(),
                                                spanProbs[i]));
                                    }
                                    model.getNameFinder().clearAdaptiveData();
                                }
                            }
                            return rows.iterator();
                        }
                    });

            List<StructField> fields = new ArrayList<>();
            StructField field = DataTypes.createStructField("database",
                    DataTypes.StringType, false);
            fields.add(field);

            field = DataTypes.createStructField("table",
                    DataTypes.StringType, false);
            fields.add(field);

            field = DataTypes.createStructField("column",
                    DataTypes.StringType, false);
            fields.add(field);

            field = DataTypes.createStructField("model",
                    DataTypes.StringType, false);
            fields.add(field);

            field = DataTypes.createStructField("probability",
                    DataTypes.DoubleType, false);
            fields.add(field);
            StructType schema = DataTypes.createStructType(fields);
            Dataset<Row> modelProbabilityData = spark.createDataFrame(modelProbability, schema);
            modelProbabilityData.registerTempTable("tempTable");
            modelProbabilityData = spark.sql("SELECT database, table, column, model, AVG(probability) AS probability from tempTable GROUP BY database, table, column, model");
            modelProbabilityData = modelProbabilityData.filter(new FilterFunction<Row>() {
                @Override
                public boolean call(Row row) throws Exception {
                    return row.getDouble(4) >= config.getProbabilityThreshold();
                }
            });
            List<Row> finalStats = modelProbabilityData.collectAsList();
            for (Row row : finalStats) {
                handle.createStatement("insert into stats(`database`, `table`, `column`, `model`, probability) values (:database, :table, :column, :model, :probability)")
                        .bind("database", row.getString(0))
                        .bind("table", row.getString(1))
                        .bind("column", row.getString(2))
                        .bind("model", row.getString(3).substring(
                                row.getString(3).lastIndexOf('-') + 1,
                                row.getString(3).indexOf('.')).toUpperCase())
                        .bind("probability", row.getDouble(4))
                        .execute();
            }
            spark.sqlContext().dropTempTable("tempTable");
            setJobStatus(handle, job.getId(), "COMPLETE");
        } catch (Exception e) {
            setJobStatus(handle, job.getId(), "FAILED");
            throw e;
        }
    }
}
