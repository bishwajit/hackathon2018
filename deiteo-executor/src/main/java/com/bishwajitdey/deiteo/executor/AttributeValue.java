package com.bishwajitdey.deiteo.executor;

import java.io.Serializable;

public class AttributeValue implements Serializable {
    private final String database;
    private final String table;
    private final String column;
    private final Object value;

    public AttributeValue(String database, String table, String column, Object value) {
        this.database = database;
        this.table = table;
        this.column = column;
        this.value = value;
    }

    public String getDatabase() {
        return database;
    }

    public String getTable() {
        return table;
    }

    public String getColumn() {
        return column;
    }

    public Object getValue() {
        return value;
    }
}
