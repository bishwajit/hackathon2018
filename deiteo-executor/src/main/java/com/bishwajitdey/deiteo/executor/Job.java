package com.bishwajitdey.deiteo.executor;


public class Job {
    private int id;
    private JobType jobType;
    private String status;
    private String configuration;

    public Job(int id, JobType jobType, String status, String configuration) {
        this.id = id;
        this.jobType = jobType;
        this.status = status;
        this.configuration = configuration;
    }

    public int getId() {
        return id;
    }

    public JobType getJobType() {
        return jobType;
    }

    public String getStatus() {
        return status;
    }

    public String getConfiguration() {
        return configuration;
    }

    @Override
    public String toString() {
        return "Job{" +
                "id=" + id +
                ", jobType=" + jobType +
                ", status='" + status + '\'' +
                '}';
    }
}
