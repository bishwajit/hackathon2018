package com.bishwajitdey.deiteo.executor;

import opennlp.tools.namefind.NameFinderME;
import opennlp.tools.namefind.TokenNameFinderModel;
import opennlp.tools.tokenize.Tokenizer;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;

import java.io.FileInputStream;
import java.io.InputStream;

public class Model {
    final private Tokenizer tokenizer;
    final private NameFinderME nameFinder;
    final private String name;

    public Model(final Tokenizer tokenizer, final NameFinderME nameFinder, final String name) {
        this.name = name;
        this.tokenizer = tokenizer;
        this.nameFinder = nameFinder;
    }

    public Tokenizer getTokenizer() {
        return this.tokenizer;
    }

    public NameFinderME getNameFinder() {
        return this.nameFinder;
    }

    public String getName() {
        return this.name;
    }

    public static Model createModel(final String modelName, final String modelFile,
                                    final String tokenModelFile) throws Exception {
        try (InputStream modelInToken = new FileInputStream(tokenModelFile);
             InputStream modelIn = new FileInputStream(modelFile)) {
            TokenizerModel tokenizerModel = new TokenizerModel(modelInToken);
            Tokenizer tokenizer = new TokenizerME(tokenizerModel);
            TokenNameFinderModel model = new TokenNameFinderModel(modelIn);
            NameFinderME nameFinder = new NameFinderME(model);
            return new Model(tokenizer, nameFinder, modelName);
        } catch (Exception e) {
            throw new Exception("Error creating model " + modelName, e);
        }
    }
}
