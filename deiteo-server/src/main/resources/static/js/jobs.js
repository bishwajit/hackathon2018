$(document).ready(function () {
    var iobAPI = "../../api/job";
    //var iobAPI = "http://localhost:8080/api/job";
    $.getJSON(iobAPI, function (result) {
        $.each(result.data, function (i, job) {
            var endTime = "";
            if(job.endTime !== null && job.endTime !== undefined){
                endTime = job.endTime;
            }
            var row = '<tr><td>' + job.id + '</td><td>' + job.jobName + '</td><td>' + job.status
                      + '</td><td>' + job.submitTime + '</td><td>' + endTime + '</td></tr>'
            $("#jobTable tbody").append(row);
        });
    });
});
