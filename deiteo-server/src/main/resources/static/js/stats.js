$(document).ready(function () {

    $( "#jobSubmitButton" ).click(function() {
        $('#statContainer').addClass("hidden");
        $('#statsTable').empty();
        var database = $("#database").val();
        var table = $("#table").val();
        var statAPI = "../../api/entity/" + database + "/" + table;
            //var tableAPI = "http://localhost:8080/api/entity";
            $.getJSON(statAPI, function (result) {
            $('<tr><th>Database</th><th>Table</th><th>Column</th><th>Category</th><th>Probability</th></tr>').appendTo('#statsTable');
                $.each(result.data, function (i, row) {
                    $('<tr><th>' + row.database + '</th><th>' + row.table + '</th><th>' + row.column + '</th><th>' + row.model + '</th><th>' + row.probability + '</th></tr>').appendTo('#statsTable');
                });
            });
            $('#statContainer').removeClass("hidden");
        return false;
    });
});
