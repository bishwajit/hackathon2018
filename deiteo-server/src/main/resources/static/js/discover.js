$(document).ready(function () {
    $('#tableContainer').addClass("hidden");
    $( "#loadTablesButton" ).click(function() {
        $('#tableContainer').addClass("hidden");
        $('#tableTree').empty();
        var tableAPI = "../../api/entity";
        //var tableAPI = "http://localhost:8080/api/entity";
        $.getJSON(tableAPI, function (result) {
            $.each(result.data, function (i, table) {
               $('<input />', { type: 'checkbox', class: 'table-names', name:table, id:table, value: table }).appendTo('#tableTree');
               $('<label />', { 'for': table, text: table, class: 'table-name-label' }).appendTo('#tableTree');
            });

            $('#tableContainer').removeClass("hidden");
        });
        return false;
    });

    $( "#jobSubmitButton" ).click(function() {
        var inputTables = $('.table-names:checkbox:checked').map(function() {
            return this.value;
        }).get();

        var modelNames = $('.model-names:checkbox:checked').map(function() {
                    return this.value;
                }).get();

        var jobConfig = {
            "inputTableName": inputTables[0],
            "models": modelNames
        };

        var jobParams = {
            "jobName": "SENSITIVITY_PREDICTION",
            "status": "PENDING",
            "configuration": JSON.stringify(jobConfig)
        };

        var jobAPI = "../../api/job";
        //var jobAPI = "http://localhost:8080/api/job";
        $.ajax({
                   url: jobAPI,
                   data: JSON.stringify(jobParams),
                   dataType:'json',
                   type:'POST',
                   contentType: 'application/json; charset=utf-8',
                   complete: function(xhr, statusText){
                       if(xhr.status == '201'){
                           alert("Successfully submitted the job.");
                       } else {
                           alert("Job submission failed. status is " + xhr.status);
                       }
                   }
               });
        return false;
    });
});
