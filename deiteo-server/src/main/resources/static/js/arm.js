$(document).ready(function () {
    var tableAPI = "../../api/entity";
    //var tableAPI = "http://localhost:8080/api/entity";
    $.getJSON(tableAPI, function (result) {
        $.each(result.data, function (i, table) {
            $('<option>').val(table).text(table).appendTo('#entitySelector');
        });
    });

    $('#entitySelector').change(function () {
        if ($(this).val() !== 'select') {
            var tableName = $(this).val();
            $.getJSON(tableAPI + "/" + tableName, function (result) {
                var schema = [];
                $.each(result.fields, function (i, field) {
                    schema.push({"text": field.name, "id": field.name})
                });

                $('#schemaTree').jstree({
                    'core' : {
                       'data' : [
                           { "text" : tableName, "id" : tableName,
                               "children" : schema, "state" : { "opened" : true },
                               "a_attr": {"class":"no_checkbox"}}
                       ],
                        "themes":{
                            "icons":false
                        }
                    },
                    "checkbox" : {
                        "three_state" : false
                    },
                    "plugins" : [ "checkbox" ]
                });

                $('#schemaContainer').removeClass("hidden");
            });
        } else {
            $('#schemaContainer').addClass("hidden");
            $('#schemaContainer').empty();
        }
    });

    $( "#jobSubmitButton" ).click(function() {
        var inputTableName = $("#entitySelector").val();
        var outputTableName = $("#outputEntityName").val();
        var itemsField = $('#schemaTree').jstree(true).get_selected(false)[0];
        var minSupport =  parseFloat($("#minSupport").val());
        var minConfidence = parseFloat($("#minConfidence").val());
        var numPartitions = parseInt($("#numPartitions").val());

        var jobConfig = {
            "inputTableName": inputTableName,
            "outputTableName": outputTableName,
            "itemsField": itemsField,
            "minSupport": minSupport,
            "minConfidence": minConfidence,
            "numPartitions": numPartitions
        };

        var jobParams = {
            "jobName": "ASSOCIATION_RULE_MINING",
            "status": "PENDING",
            "configuration": JSON.stringify(jobConfig)
        };

        var jobAPI = "../../api/job";
        //var jobAPI = "http://localhost:8080/api/job";
        $.ajax({
                   url: jobAPI,
                   data: JSON.stringify(jobParams),
                   dataType:'json',
                   type:'POST',
                   contentType: 'application/json; charset=utf-8',
                   complete: function(xhr, statusText){
                       if(xhr.status == '201'){
                           alert("Successfully submitted the job.");
                       } else {
                           alert("Job submission failed. status is " + xhr.status);
                       }
                   }
               });
        return false;
    });
});
