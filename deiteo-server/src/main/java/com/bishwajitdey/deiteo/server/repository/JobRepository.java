package com.bishwajitdey.deiteo.server.repository;

import com.bishwajitdey.deiteo.server.entities.Job;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface JobRepository extends JpaRepository<Job, Long> {
    Job findOneById(long id);

    List<Job> findAllByOrderBySubmitTimeDesc();
}


