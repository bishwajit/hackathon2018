package com.bishwajitdey.deiteo.server.repository;

import com.bishwajitdey.deiteo.server.entities.Stats;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface StatRepository extends JpaRepository<Stats, Long> {
    Stats findOneById(long id);

    List<Stats> findByDatabaseAndTable(String database, String table);
}


