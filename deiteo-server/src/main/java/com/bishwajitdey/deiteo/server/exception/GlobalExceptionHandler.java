package com.bishwajitdey.deiteo.server.exception;


import com.bishwajitdey.deiteo.server.web.rest.dto.GeneralResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {
    public static final Logger LOGGER = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @ExceptionHandler(NotFoundException.class)
    @ResponseBody
    public ResponseEntity<GeneralResponse> handleEntityNotFoundException(NotFoundException ex) {
        LOGGER.error("Requested resource not found", ex);
        GeneralResponse responseEntity = new GeneralResponse(ex.getMessage());
        return new ResponseEntity<>(responseEntity, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseBody
    public ResponseEntity<GeneralResponse> handleIllegalArgumentException(
            IllegalArgumentException ex) {
        LOGGER.error("Invalid request sent.", ex);
        GeneralResponse responseEntity = new GeneralResponse(ex.getMessage());
        return new ResponseEntity<>(responseEntity, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public ResponseEntity<GeneralResponse> handleAnyException(Exception ex) {
        LOGGER.error("Could not process request.", ex);
        GeneralResponse responseEntity = new GeneralResponse(ex.getMessage());
        return new ResponseEntity<>(responseEntity, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
