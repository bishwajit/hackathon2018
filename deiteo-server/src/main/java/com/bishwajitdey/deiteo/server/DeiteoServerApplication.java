package com.bishwajitdey.deiteo.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DeiteoServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(DeiteoServerApplication.class, args);
    }
}
