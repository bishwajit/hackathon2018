package com.bishwajitdey.deiteo.server.web.rest;

import com.bishwajitdey.deiteo.server.entities.Stats;
import com.bishwajitdey.deiteo.server.entities.Table;
import com.bishwajitdey.deiteo.server.service.EntityService;
import com.bishwajitdey.deiteo.server.service.StatService;
import com.bishwajitdey.deiteo.server.web.rest.dto.CollectionWrapperDTO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.Callable;

@CrossOrigin
@RestController
@RequestMapping(value = "/api/entity", produces = MediaType.APPLICATION_JSON_VALUE)
public class EntityResource {
    private final EntityService entityService;
    private final StatService statService;

    @Autowired
    public EntityResource(EntityService entityService, StatService statService) {
        this.entityService = entityService;
        this.statService = statService;
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public Callable<ResponseEntity<CollectionWrapperDTO<String>>> getAllTables() {
        return new Callable<ResponseEntity<CollectionWrapperDTO<String>>>() {
            @Override
            public ResponseEntity<CollectionWrapperDTO<String>> call() throws Exception {
                return new ResponseEntity<>(new CollectionWrapperDTO<>(entityService.getAllTables()),
                        HttpStatus.OK);
            }
        };
    }

    @RequestMapping(value = "/{table}", method = RequestMethod.GET)
    @ResponseBody
    public Callable<ResponseEntity<Table>> getTableInfo(final @PathVariable("table") String table) {
        return new Callable<ResponseEntity<Table>>() {
            @Override
            public ResponseEntity<Table> call() throws Exception {
                return new ResponseEntity<>(entityService.getTableInfo(table), HttpStatus.OK);
            }
        };
    }

    @RequestMapping(value = "/{database}/{table}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<CollectionWrapperDTO<Stats>> getStatForTable(
            final @PathVariable("database") String database,
            final @PathVariable("table") String table) {
        return new ResponseEntity<>(new CollectionWrapperDTO<>(statService.getStatsByDatabaseAndTable(
                database, table
        )),
                HttpStatus.OK);
    }
}
