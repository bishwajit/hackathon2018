package com.bishwajitdey.deiteo.server.service;

import com.bishwajitdey.deiteo.server.entities.Job;
import com.bishwajitdey.deiteo.server.repository.JobRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class JobService {
    @Autowired
    private JobRepository jobRepository;

    public Job getJobById(long id) {
        return jobRepository.findOneById(id);
    }

    public List<Job> getAllJobs() {
        return jobRepository.findAllByOrderBySubmitTimeDesc();
    }

    public Job createJob(Job job) {
        return jobRepository.save(job);
    }
}
