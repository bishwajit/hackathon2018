package com.bishwajitdey.deiteo.server.web.rest;

import com.bishwajitdey.deiteo.server.entities.Job;
import com.bishwajitdey.deiteo.server.exception.NotFoundException;
import com.bishwajitdey.deiteo.server.service.JobService;
import com.bishwajitdey.deiteo.server.web.rest.dto.CollectionWrapperDTO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.Date;

@CrossOrigin
@RestController
@RequestMapping(value = "/api/job", produces = MediaType.APPLICATION_JSON_VALUE)
public class JobResource {
    private final JobService jobService;

    @Autowired
    public JobResource(JobService jobService) {
        this.jobService = jobService;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Job> getJob(
            @PathVariable("id") Integer id) {
        Job job = jobService.getJobById(id);
        if (job == null) {
            throw new NotFoundException("Job with id " + id + " does not exist.");
        }
        return new ResponseEntity<>(job, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<CollectionWrapperDTO<Job>> getAllJobs() {
        return new ResponseEntity<>(new CollectionWrapperDTO<>(jobService.getAllJobs()),
                HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<?> createJob(
            @RequestBody Job job) {
        job.setSubmitTime(new Date(System.currentTimeMillis()));
        Job createdJob = jobService.createJob(job);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setLocation(ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{id}")
                .buildAndExpand(createdJob.getId()).toUri());
        return new ResponseEntity<>(null, httpHeaders, HttpStatus.CREATED);
    }
}
