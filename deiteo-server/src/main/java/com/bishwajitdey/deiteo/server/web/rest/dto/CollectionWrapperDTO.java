package com.bishwajitdey.deiteo.server.web.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import org.hibernate.validator.constraints.NotEmpty;

import java.util.Collection;

public class CollectionWrapperDTO<T> {
    Collection<T> data;

    public CollectionWrapperDTO() {
    }

    public CollectionWrapperDTO(Collection<T> data) {
        this.data = data;
    }

    @JsonProperty
    public Collection<T> getData() {
        return data;
    }

    public void setData(Collection<T> data) {
        this.data = data;
    }
}
