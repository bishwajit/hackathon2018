package com.bishwajitdey.deiteo.server.service;

import com.bishwajitdey.deiteo.server.entities.Field;
import com.bishwajitdey.deiteo.server.entities.Table;
import com.bishwajitdey.deiteo.server.exception.ServiceException;

import org.apache.hadoop.hive.conf.HiveConf;
import org.apache.hadoop.hive.metastore.HiveMetaStoreClient;
import org.apache.hadoop.hive.metastore.api.FieldSchema;
import org.apache.hadoop.hive.metastore.api.MetaException;
import org.apache.thrift.TException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

@Service
@Transactional
public class EntityService {
    private final HiveMetaStoreClient hiveMetaStoreClient;
    @Autowired
    Environment environment;

    public EntityService() {
        //hiveMetaStoreClient = null;
        try {
            hiveMetaStoreClient = new HiveMetaStoreClient(new HiveConf());
        } catch (MetaException e) {
            throw new ServiceException("Meta store connection issue.", e);
        }
    }

    public Collection<String> getAllTables() {
        try {
            final String dbName = environment.getProperty("sensitivo.hive.db", "default");
            return hiveMetaStoreClient.getAllTables(dbName);
        } catch (MetaException e) {
            throw new ServiceException("Meta store connection issue.", e);
        }
    }

    public Table getTableInfo(String tableName) {
        try {
            final String dbName = environment.getProperty("sensitivo.hive.db", "default");
            List<FieldSchema> schema = hiveMetaStoreClient.getSchema(dbName, tableName);
            List<Field> fields = new LinkedList<>();
            for (FieldSchema fieldSchema : schema) {
                Field field = new Field();
                field.setName(fieldSchema.getName());
                field.setType(fieldSchema.getType());
                fields.add(field);
            }
            Table table = new Table();
            table.setName(tableName);
            table.setFields(fields);
            return table;
        } catch (TException e) {
            throw new ServiceException("Meta store connection issue.", e);
        }
    }
}
