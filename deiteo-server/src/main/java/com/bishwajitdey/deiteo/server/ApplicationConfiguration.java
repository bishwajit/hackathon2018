package com.bishwajitdey.deiteo.server;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableJpaRepositories("com.bishwajitdey.deiteo.server.repository")
@EnableTransactionManagement
public class ApplicationConfiguration {
}
