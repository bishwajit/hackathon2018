package com.bishwajitdey.deiteo.server.web.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Representation of general response from the server when no specific data need to be returned.
 */
public class GeneralResponse {
    private String message;

    public GeneralResponse() {
    }

    public GeneralResponse(String message) {
        this.message = message;
    }

    @JsonProperty
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
