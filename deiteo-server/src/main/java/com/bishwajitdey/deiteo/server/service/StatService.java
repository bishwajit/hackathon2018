package com.bishwajitdey.deiteo.server.service;

import com.bishwajitdey.deiteo.server.entities.Stats;
import com.bishwajitdey.deiteo.server.repository.StatRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class StatService {
    @Autowired
    private StatRepository statRepository;

    public List<Stats> getStatsByDatabaseAndTable(String database, String table) {
        return statRepository.findByDatabaseAndTable(database, table);
    }
}
